# Rest_Assured_API_Testing_Framework


Please Note as explained in other articles on page for security purpose I have changed the keys used in Google API, JIRA and Twitter API. So for using this you need to generate your own private key as I explained in articles and use it.

Below are some of the important feature of this rest assured API Testing framework

Programming Language Used: JAVA
JDK Version:1.8
Browser: Chrome (Code will work in all browser just change the browser name in Config file)
Framework Used: TestNg with Data driven approach
Design Pattern: you can implement this with either with cucumber or with page object.
Project Type: Maven
Loggers:Log4j
Request and Response Loggers for easy tracking
Screenshot: TakeScreenshot Interface
Listners: RetryListner,WebElement Listner and ITestListner
Reporting : TestNg Reports and Extent Report
Code Checkin : Git Repository
CICD Tools: Jenkins
 
You can execute all the test either through Command prompt, Eclipse Console, Maven Command Line or Maven Eclipse, Testng or through Jenkins i have validate the code through all the possible ways.
 

